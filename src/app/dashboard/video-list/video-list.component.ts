import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Video } from '../types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent {
  @Input() videosList?: Video[];
  selectedVideoId: Observable<string>;

  constructor(route: ActivatedRoute, private router: Router) {
    this.selectedVideoId = route.queryParamMap.pipe(
      map(params => params.get('id') as string),
    );
  }

  selectVideo(video: Video) {
    this.router.navigate([], { queryParams: { id: video.id } });
  }
}
