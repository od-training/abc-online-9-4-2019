import { Component } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

import { Video } from '../types';
import { VideoDataService } from '../video-data.service';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent {
  allVideos: Observable<Video[]>;
  filters: FormGroup;
  selectedVideo: Observable<Video | undefined>;

  constructor(vds: VideoDataService, fb: FormBuilder, route: ActivatedRoute) {
    this.allVideos = vds.getVideos();
    this.filters = fb.group({
      region: ['all'],
      fromDate: ['2000-01-01'],
      toDate: ['2020-01-01'],
      under18: [true],
      '18to40': [true],
      '40to60': [true],
      over60: [true]
    });
    const selectedVideoId = route.queryParamMap.pipe(
      map(params => params.get('id') as string),
    );
    this.selectedVideo = combineLatest(this.allVideos, selectedVideoId).pipe(
      map(([videos, id]) => videos.find(v => v.id === id)),
    );
  }
}
