import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

import { Video } from './types';

/** Return a new array of videos with all the titles upper-cased */
function upperCaseTitles(videos: Video[]) {
  return videos.map((v: Video) => ({
    ...v,
    title: v.title.toUpperCase()
  }));
}

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {
  constructor(private http: HttpClient) { }

  getVideos(): Observable<Video[]> {
    return this.http
      .get<Video[]>('https://api.angularbootcamp.com/videos')
      .pipe(
        map((videos: Video[]) => upperCaseTitles(videos)),
        shareReplay(1),
      );
  }
}
